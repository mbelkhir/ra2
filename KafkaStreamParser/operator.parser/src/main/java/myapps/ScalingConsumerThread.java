package myapps;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Producer;

/**
 *
 * @author mbelkhir
 */
public class ScalingConsumerThread extends Thread {

    Properties props = new Properties();
    ProtocolGME protocol;
    Repertory repertory;
    KafkaParserSecondNode node2;
    Producer producer;
    KafkaConsumer<String, MessageGME> consumer;
    Producer prodDeploy;
    Object ob;

    public ScalingConsumerThread(Repertory repertory, KafkaParserSecondNode node2, Producer producer, ProtocolGME protocol, Object ob) {
        this.repertory = repertory;
        this.node2 = node2;
        this.producer = producer;
        this.protocol = protocol;
        this.ob = ob;
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "consumer" + node2.id);
        props.put("value.deserializer", "myapps.MessageGMEDeserializer");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("auto.offset.reset", "earliest");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("max.poll.records", "1");
        consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(node2.id + ""));
    }

    @Override
    public void run() {
        while (true) {
            ConsumerRecords<String, MessageGME> records = consumer.poll(Duration.ofMillis(10));
            for (ConsumerRecord<String, MessageGME> record : records) {
                //System.err.println(record.value().toString());
                protocol.onReceiptOf(record.value(), repertory, node2, producer, ob);
            }
        }
    }
}

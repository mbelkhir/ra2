package myapps;

import java.util.Vector;

public class Priority {

    public int x;
    public Vector<Integer> y;

    public Priority(int x, Vector<Integer> y) {
        this.x = x;
        this.y = y;
    }

    public Boolean lessThan(Priority priority) {
        if (y.get(0) == Integer.MAX_VALUE) {
            return false;
        } else if (priority.y.get(0) == Integer.MAX_VALUE) {
            return true;
        }
        int somme1 = 0;
        int somme2 = 0;
        for (int i = 0; i < y.size(); i++) {

            somme1 = somme1 + y.get(i);
            somme2 = somme2 + priority.y.get(i);
        }

        if (somme1 < somme2) {
            return true;
        } else if (somme1 > somme2) {
            return false;
        } else if (somme1 == somme2) {
            if (x < priority.x) {
                return true;
            } else if (x > priority.x) {
                return false;
            } else {
                System.err.println("Error & exit (two priority are equals");
                System.exit(0);
            }
        }
        return null;
    }

    public Boolean lessOrEqualThan(Priority priority) {
        if (y.get(0) == Integer.MAX_VALUE) {
            return false;
        } else if (priority.y.get(0) == Integer.MAX_VALUE) {
            return true;
        }
        int somme1 = 0;
        int somme2 = 0;
        for (int i = 0; i < y.size(); i++) {
            somme1 = somme1 + y.get(i);
            somme2 = somme2 + priority.y.get(i);
        }

        if (somme1 <= somme2) {
            return true;
        } else if (somme1 >= somme2) {
            return false;
        }
        return null;
    }
}

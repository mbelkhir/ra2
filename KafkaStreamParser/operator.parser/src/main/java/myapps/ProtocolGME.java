package myapps;

import static java.lang.Integer.max;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.Vector;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class ProtocolGME {

    public Vector<Integer> maxVSN(Vector<Integer> v1, Vector<Integer> v2, Repertory repertory) {
        Vector<Integer> rst = new Vector<Integer>();
        for (int i = 0; i < repertory.getAllNodes().size(); i++) {
            rst.add(0);
        }
        for (int i = 0; i < repertory.getAllNodes().size(); i++) {
            rst.set(i, max(v1.get(i), v2.get(i)));
        }
        return rst;
    }

    public void onReceiptOf(MessageGME msg, Repertory repertory, KafkaParserSecondNode node, Producer producer, Object ob) {
        synchronized (ob) {
            switch (msg.getType()) {
                case "REQ": {
                    System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                            + " id:" + node.id + " RCV:REQ FROM:" + msg.getSrc()
                            + " VSN_i'j' = " + msg.getVSN() + " priority:" + node.priority.y + " end");
                    Priority msgPriority = new Priority(Integer.parseInt(msg.getSrc()), msg.getVSN());
                    node.VSN = maxVSN(node.VSN, msg.getVSN(), repertory);
                    Iterator<MessageGME> itr = node.requestSet.iterator();
                    while (itr.hasNext()) {
                        MessageGME reqSet = itr.next();
                        if (msg.getSrc().equals(reqSet.getSrc())) {
                            itr.remove();
                        }
                    }
                    if (node.VF.get(Integer.parseInt(msg.getSrc())) < msg.getVSN().get(Integer.parseInt(msg.getSrc()))) {
                        //otherwise the request is out of date
                        if (!node.requestSet.contains(msg)) {
                            node.requestSet.add(msg);
                        }
                        Boolean bool1 = node.target.equals(msg.getTarget()) && (!node.isCaptain);
                        Boolean bool2 = (!node.target.equals(msg.getTarget())) && (!node.state.equals("talking"))
                                && (msgPriority.lessThan(node.priority));
                        if (bool1 || bool2) {
                            System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                    + " id:" + node.id + " SEND:ACK_TO:" + msg.getSrc()
                                    + " VSN_i'j' = " + msg.getVSN() + " priority:" + node.priority.y + " end");
                            Vector<Integer> aux = new Vector<>();
                            aux.add(msg.getVSN().get(Integer.parseInt(msg.getSrc())));
                            System.out.println("#SEND end");
                            ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(msg.getSrc(),
                                    new MessageGME(node.target, aux, node.VF, msg.getSrc(), node.id + "", "ACK"));
                            producer.send(rec);
                        } else if (node.target.equals(msg.getTarget()) && node.isCaptain) {
                            Vector<Integer> aux = new Vector<>();
                            aux.add(msg.getVSN().get(Integer.parseInt(msg.getSrc())));
                            System.out.println("#SEND end");
                            ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(msg.getSrc(),
                                    new MessageGME(node.target, node.priority.y, aux, msg.getSrc(), node.id + "", "START"));
                            producer.send(rec);
                            //System.err.println("id:" + node.id + " VF:" + node.VF + " REQ_before_start");
                            //node i is sure that node j will make its vsn_j[j] th entry to the meeting room
                            node.VF.set(Integer.parseInt(msg.getSrc()), max(node.VF.get(Integer.parseInt(msg.getSrc())),
                                    msg.getVSN().get(Integer.parseInt(msg.getSrc()))));
                            //System.err.println("id:" + node.id + " VF:" + node.VF + " REQ_after_start");
                        }
                        if (node.target.equals(msg.getTarget()) && node.state.equals("talking")) {
                            if (!node.friendRequest.contains(msg)) {
                                node.friendRequest.add(msg);
                            }
                        }
                    }
                    break;
                }
                case "ACK": {
                    System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                            + " id:" + node.id + " RCV:ACK FROM:" + msg.getSrc() + " VFj:" + msg.getVF() + " VFi:" + node.VF + " end");
                    //System.err.println("id:" + node.id + " VF:" + node.VF + " before_ack from:" + msg.getSrc() + " VFj:" + msg.getVF());
                    node.VF = maxVSN(node.VF, msg.getVF(), repertory);
                    //System.err.println("id:" + node.id + " VF:" + node.VF + " after_ack" + msg.getSrc() + " VFj:" + msg.getVF());
                    if (Objects.equals(node.VSN.get(node.id), msg.getVSN().get(0)) && !node.target.equals("talking")) {
                        Iterator<MessageGME> itr = node.acksSet.iterator();
                        while (itr.hasNext()) {
                            MessageGME acksSet = itr.next();
                            if (msg.getSrc().equals(acksSet.getSrc())) {
                                System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                        + " id:" + node.id + " REMOVE:ACK_SET(previous_reply):" + acksSet.getSrc() + " end");
                                itr.remove();
                            }
                        }
                        if (!node.acksSet.contains(msg)) {
                            node.acksSet.add(msg);
                        }
                        Iterator<MessageGME> itrAck = node.acksSet.iterator();
                        while (itrAck.hasNext()) {
                            MessageGME acksSet = itrAck.next();
                            if (acksSet.getVF().get(Integer.parseInt(acksSet.getSrc()))
                                    < node.VF.get(Integer.parseInt(acksSet.getSrc()))) {
                                System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                        + " id:" + node.id + " REMOVE:ACK_SET(out-of-date):" + acksSet.getSrc() + " end");
                                itrAck.remove();
                            }
                        }
                        if (node.acksSet.size() == repertory.getAllNodes().size() - 1) {
                            System.out.print("time:" + (new java.util.Date().getTime() - node.startTime)
                                    + " id:" + node.id + " state:ENTER_CS " + "Group:" + node.target + " end");
                            node.state = "talking";
                            node.VF.set(node.id, node.VF.get(node.id) + 1);
                            node.isCaptain = true;
                            node.acksSet = new ArrayList<>();
                            Iterator<MessageGME> itrReq = node.requestSet.iterator();
                            while (itrReq.hasNext()) {
                                MessageGME reqSet = itrReq.next();
                                if (node.target.equals(reqSet.getTarget())
                                        && node.VF.get(Integer.parseInt(reqSet.getSrc()))
                                        < reqSet.getVSN().get(Integer.parseInt(reqSet.getSrc()))) {
                                    Vector<Integer> aux = new Vector<>();
                                    aux.add(reqSet.getVSN().get(Integer.parseInt(reqSet.getSrc())));
                                    System.out.println("#SEND end");
                                    ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(reqSet.getSrc(),
                                            new MessageGME(node.target, node.priority.y, aux, reqSet.getSrc(), node.id + "", "START"));
                                    producer.send(rec);
                                    //System.err.println("id:" + node.id + " VF:" + node.VF + " ACK_before_sending_start");
                                    node.VF.set(Integer.parseInt(reqSet.getSrc()),
                                            max(node.VF.get(Integer.parseInt(reqSet.getSrc())), reqSet.getVSN().get(Integer.parseInt(reqSet.getSrc()))));
                                    //System.err.println("id:" + node.id + " VF:" + node.VF + " ACK_after_sending_start");
                                } else if (new Priority(Integer.parseInt(reqSet.getSrc()), reqSet.getVSN()).lessThan(node.priority)) {
                                    itrReq.remove();
                                }
                            }
                            ob.notify();
                        }
                    }
                }
                break;
                case "START": {
                    System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                            + " id:" + node.id + " RCV:START FROM:" + msg.getSrc() + " end");
                    if (Objects.equals(node.VSN.get(node.id), msg.getVF().get(0)) && !node.state.equals("talking")) {
                        System.out.print("time:" + (new java.util.Date().getTime() - node.startTime)
                                + " id:" + node.id + " state:ENTER_CS " + "Group:" + node.target + " end");
                        node.state = "talking";
                        node.VF.set(node.id, node.VF.get(node.id) + 1);
                        node.acksSet = new ArrayList<>();
                        Iterator<MessageGME> itrReq = node.requestSet.iterator();
                        while (itrReq.hasNext()) {
                            MessageGME reqSet = itrReq.next();
                            if (new Priority(Integer.parseInt(reqSet.getSrc()),
                                    reqSet.getVSN()).lessOrEqualThan(new Priority(Integer.parseInt(msg.getSrc()), msg.getVSN()))) {
                                itrReq.remove();
                            }
                        }
                        ob.notify();
                    }
                    break;
                }
                default:
                    //System.err.println("message not recognized");
                    break;
            }
        }
    }
}

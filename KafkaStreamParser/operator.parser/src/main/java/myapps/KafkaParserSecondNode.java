package myapps;

import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;

public class KafkaParserSecondNode {

    public long startTime = new java.util.Date().getTime();
    //mettre le temps de référance +/- % id
    public Random random = new Random(startTime);
    //change ti min / max
    //0 et max (para)
    public int inCS;
    public int freqToRequestCS;
    public int tempOfSimulation = 2;

    public static String cluster;

    //Variables used for scaling
    public boolean isLeader = true;
    public boolean isActive = true;

    public int id;

    public String state = "thinking";
    public Vector<Integer> VSN;
    public Vector<Integer> VF;
    public Priority priority;
    public String target;
    public Boolean isCaptain = false;
    public ArrayList<MessageGME> requestSet;
    public ArrayList<MessageGME> friendRequest;
    public ArrayList<MessageGME> acksSet;

    public KafkaParserSecondNode(int id, String group, String startTimeint, int inCS, int freqToRequestCS) {   
        this.state = "thinking";
        VSN = new Vector<>();
        VF = new Vector<>();
        Vector<Integer> infinity = new Vector<>();
        this.isLeader = true;
        this.isActive = true;
        this.cluster = "localhost:9092";
        this.id = id;
        this.target = group;
        this.startTime = Long.parseLong(startTimeint);
        this.inCS = inCS;
        this.freqToRequestCS = freqToRequestCS;
        for (int i = 0; i < new Repertory().getAllNodes().size(); i++) {
            VSN.add(0);
            VF.add(0);
            infinity.add(Integer.MAX_VALUE);
        }
        requestSet = new ArrayList<>();
        friendRequest = new ArrayList<>();
        acksSet = new ArrayList<>();
        priority = new Priority(id, infinity);
    }

    public static void main(String[] args) {

        //System.out.println(seed);
        Object o = new Object();

        ProtocolGME protocol = new ProtocolGME();
        KafkaParserSecondNode node;
        Repertory repertory = new Repertory();

        node = new KafkaParserSecondNode(Integer.parseInt(args[0]), args[1], args[2],
                Integer.parseInt(args[3]), Integer.parseInt(args[4]));
        /**
         * Producer's configuration for consumer (scaling algorithm)*
         */
        Properties scalingprops = new Properties();
        scalingprops.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        scalingprops.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        scalingprops.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonPOJOSerializer.class.getName());
        //scalingprops.put(ProducerConfig.ACKS_CONFIG, "0");
        Producer prodScaling = new KafkaProducer<>(scalingprops);

        ScalingConsumerThread scalingMsg = new ScalingConsumerThread(repertory, node, prodScaling, protocol, o);
        scalingMsg.start();
        RequestCS requestCS = new RequestCS(repertory, node, prodScaling, o);
        requestCS.start();
    }
}

package myapps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class RequestCS extends Thread {

    Repertory repertory;
    KafkaParserSecondNode node;
    Producer producer;
    Object o;
    Integer temp = 0;

    public RequestCS(Repertory repertory, KafkaParserSecondNode node, Producer producer, Object ob) {
        this.repertory = repertory;
        this.node = node;
        this.producer = producer;
        o = ob;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
            //Converger la vitesse de kafka consumer
            repertory.getAllNodes().stream().map((succ) -> {
                ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(succ,
                        new MessageGME("Hi", null, null, succ, node.id + "", "Hi"));
                return rec;
            }).forEachOrdered((rec) -> {
                producer.send(rec);
            });
            Thread.sleep(2000);
            while (true) {
                //int frequency = (int) (3000 + Math.random() * (30000 + 1));

                Thread.sleep(node.freqToRequestCS);

                if (temp >= node.tempOfSimulation) {
                    //send msg end
                    System.out.println("#finished");
                    sleep(20000);
                    System.exit(0);
                }
                synchronized (o) {
                    //if (node.state == "thinking") {                   
                    temp++;
                    System.err.println("node" + node.id + ":" + temp);
                    node.state = "waiting";
                    node.VSN.set(node.id, node.VSN.get(node.id) + 1);
                    node.priority = new Priority(node.id, node.VSN);
                    System.out.println("time:" + (new java.util.Date().getTime() - node.startTime)
                            + " id:" + node.id + " state:REQ " + "Group:" + node.target + " VSN:" + node.VSN + " end");
                    for (String nd : repertory.getAllNodes()) {
                        if (!nd.equals(node.id + "")) {
                            System.out.println("#SEND end");
                            ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(nd,
                                    new MessageGME(node.target, node.priority.y, null, nd, node.id + "", "REQ"));
                            producer.send(rec);
                        }
                    }
                    // }
                    o.wait();
                }

                Thread.sleep(node.inCS);
                synchronized (o) {
                    System.out.println("time:" + (new java.util.Date().getTime() - node.startTime) + " id:"
                            + node.id + " state:LEAVE_CS " + "Group:" + node.target + " end");
                    Vector<Integer> infinity = new Vector<>();
                    for (int i = 0; i < repertory.getAllNodes().size(); i++) {
                        infinity.add(Integer.MAX_VALUE);
                    }
                    node.friendRequest = new ArrayList<>();
                    node.priority = new Priority(node.id, infinity);
                    node.isCaptain = false;
                    node.state = "thinking";
                    Iterator<MessageGME> itr = node.requestSet.iterator();
                    while (itr.hasNext()) {
                        MessageGME reqSet = itr.next();
                        if ((node.VF.get(Integer.parseInt(reqSet.getSrc())) < reqSet.getVSN().get(Integer.parseInt(reqSet.getSrc())))
                                && (!node.friendRequest.contains(reqSet))) {
                            Vector<Integer> aux = new Vector<>();
                            aux.add(reqSet.getVSN().get(Integer.parseInt(reqSet.getSrc())));
                            System.out.println("#SEND end");
                            ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(reqSet.getSrc(),
                                    new MessageGME(node.target, aux, node.VF, reqSet.getSrc(), node.id + "", "ACK"));
                            producer.send(rec);
                        } else if (node.VF.get(Integer.parseInt(reqSet.getSrc())) >= reqSet.getVSN().get(Integer.parseInt(reqSet.getSrc()))) {
                            //the request is out of date
                            itr.remove();
                        }
                    }
                }
            }
        } catch (InterruptedException ex) {
        }
    }
}

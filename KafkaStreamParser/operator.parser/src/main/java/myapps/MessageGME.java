package myapps;

import java.util.Vector;

public class MessageGME {

    //Variables used to be send in REQ & ACK for GME messages   
    private String target;
    private Vector<Integer> VSN;
    private Vector<Integer> VF;
    private String dest;
    private String src;
    private String type;

    public MessageGME() {
    }

    public MessageGME(String target, Vector<Integer> VSN, Vector<Integer> VF, String dest, String src, String type) {
        this.target = target;
        this.VSN = VSN;
        this.VF = VF;
        this.dest = dest;
        this.src = src;
        this.type = type;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Vector<Integer> getVSN() {
        return VSN;
    }

    public void setVSN(Vector<Integer> VSN) {
        this.VSN = VSN;
    }

    public Vector<Integer> getVF() {
        return VF;
    }

    public void setVF(Vector<Integer> VF) {
        this.VF = VF;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        MessageGME guest = (MessageGME) obj;
        return (VSN == guest.VSN || (VSN != null && VSN.equals(guest.getVSN())))
                && ((target == null ? guest.target == null : target.equals(guest.target)) || (target != null && target.equals(guest.getTarget())))
                && ((dest == null ? guest.dest == null : dest.equals(guest.dest)) || (dest != null && dest.equals(guest.getDest())))
                && ((src == null ? guest.src == null : src.equals(guest.src)) || (src != null && src.equals(guest.getSrc())))
                && ((type == null ? guest.type == null : type.equals(guest.type)) || (type != null && type.equals(guest.getType())))
                && (VF == guest.VF || (VF != null && VF.equals(guest.getVF())));
    }

    @Override
    public String toString() {
        return "MessageGME{" + "target=" + target + ", VSN=" + VSN + ", VF=" + VF + ", dest=" + dest + ", src=" + src + ", type=" + type + '}';
    }

}
